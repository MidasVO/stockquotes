'use strict';

var Table, tb, url, sq, Stock, window, xmlhttp, XMLHttpRequest, ActiveXObject;

(function () {

  var bodyNode;
  /**
   * Contains create and update functions of table
   * @type {Object}
   */
  Table = {
    /**
     * Creates and fills the table with data
     * @param  {[type]} stocks
     * @return {[type]}
     */
    create: function (stocks) {
      console.log("Creating table...");
      var s = stocks, h, table, td, name, title, text, th, row, j, i;

      bodyNode = document.getElementById("app");

      // create table  
      table = document.createElement("table");
      table.id = "table";
      bodyNode.appendChild(table);

      var myrow = document.createElement("tr");
      table.appendChild(myrow);
      // create headers
      for (h = 0; h < 5; h++) {
        if (h === 0) {
          title = "Naam";
        } else if (h === 1) {
          title = " ";
        } else if (h === 2) {
          title = "Koers";
        } else if (h === 3) {
          title = "+/-";
        } else if (h === 4) {
          title = "% +/-";
        }
        text = document.createTextNode(title);
        th = document.createElement("th");
        th.appendChild(text);
        myrow.appendChild(th);
      }


      // create rows
      for (i = 1; i < 25; i++) {
        row = document.createElement("tr");
        row.id = "row-" + i;
        row.style.border = "solid black";

        table.appendChild(row);

        for (j = 0; j < 5; j++) {
          if (j === 0) {
            td = document.createElement("td");
            row.appendChild(td);

            name = document.createTextNode(s[i].name);
            td.appendChild(name);
          } else if (j === 1) {
            td = document.createElement("td");
            row.appendChild(td);

            name = document.createTextNode(s[i].state);
            td.appendChild(name);

          } else if (j === 2) {
            tb.td(row, s[i].value);

          } else if (j === 3) {
            td = document.createElement("td");
            row.appendChild(td);

            name = document.createTextNode(s[i].change);
            td.appendChild(name);

          } else if (j === 4) {
            td = document.createElement("td");
            row.appendChild(td);

            name = document.createTextNode(s[i].changep);
            td.appendChild(name);

          }
        }
        j = 0;
      }
      console.log("/-/-/-/-/-/-/-/-/-/-/");
    },



    /**
     * Updates the table using the stocks object
     * @param  {[type]} stocks
     * @return {[type]}
     */
    update: function (stocks) {
      var i, x, tableNode, amountStocks;
      console.log("Updating table...");
      tableNode = document.getElementById('table');
      amountStocks = Object.size(stocks);


      for (i = 1; i < amountStocks; i++) {
        x = tableNode.rows[i].cells;
        x[2].innerHTML = stocks[i].value;
        x[3].innerHTML = stocks[i].change;
        x[4].innerHTML = stocks[i].changep;
      }

    },


/**
 * Creates td
 * @param  {[type]} row
 * @param  {[type]} attr
 * @return {[type]}
 */
    td: function (row, attr) {
      var td = document.createElement("td");
      row.appendChild(td);

      var name = document.createTextNode(attr);
      td.appendChild(name);

    },

  }; // END TABLE




/**
 * Contains functions to initialize and creating the stocks object
 * @type {Object}
 */
  var stockQuotes = {
    // create table
    init: function () {
      console.log("init...");
      url.fetch();
      setInterval(function () {
        console.log("/-/-/-/-/-/-/-/-/-/-/");
        url.fetch();
      }, 5000);
    },

/**
 * Creates stocks from quotes
 * @param  {[type]} quotes
 * @return {[type]}
 */
    createStocks: function (quotes) {
      var amountQuotes, stocks, q, symbol, name, change, changep, value, state, s, i;
      console.log("Creating stocks...");
      amountQuotes = quotes.length;
      stocks = {};


      for (i = 0; i < amountQuotes; i++) {
        if (quotes.hasOwnProperty(i)) {
          q = quotes[i];
          symbol = q.Symbol;
          name = q.Name;
          change = q.Change;
          changep = q.ChangeinPercent;
          value = q.BookValue;
          s = change;
          if (s.indexOf("-") !== -1) { // check if string contains '-'
            state = "down";
          } else {
            state = "up";
          }
          stocks[i] = new Stock(q, value, symbol, name, change, changep, state);
        }
      }


      if (!document.getElementById('table')) { // check if there is a table
        tb.create(stocks);
      } else {
        tb.update(stocks);
      }
    }

  }; // end var stockQuotes
/**
 * Contains function to fetch quotes from yahoo
 * @type {Object}
 */
  var URL = {
/**
 * Creates xmlhttprequest
 * @return {[type]}
 */
    fetch: function () {
      var thisurl;
      console.log("Fetching data...");
      thisurl = "http://query.yahooapis.com/v1/public/yql?q=select%20Change%2C%20Symbol%2C%20Name%2C%20ChangeinPercent%2C%20BookValue%20from%20yahoo.finance.quotes%20where%20symbol%20in%0A(%22BCS%22%2C%22STT%22%2C%22JPM%22%2C%22LGEN.L%22%2C%22UBS%22%2C%22DB%22%2C%22BEN%22%2C%22CS%22%2C%22BK%22%2C%22KN.PA%22%2C%22GS%22%2C%22LM%22%2C%22MS%22%2C%22MTU%22%2C%22NTRS%22%2C%22GLE.PA%22%2C%22BAC%22%2C%22AV%22%2C%22SDR.L%22%2C%22DODGX%22%2C%22SLF%22%2C%22SL.L%22%2C%22NMR%22%2C%22ING%22%2C%22BNP.PA%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
      if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }

      xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4) { // we have a response
          var json = JSON.parse(this.responseText);
          var quotes = json.query.results.quote;
          // alert("json " + json[0]);
          // alert("quotes " + quotes[0]['Name']);
          sq.createStocks(quotes);

        }

      };

      xmlhttp.open('GET', thisurl, true);
      xmlhttp.send(null);
      console.log("Data fetching complete!");

    }
  }; // end var url


  window.sq = stockQuotes;
  window.tb = Table;
  window.url = URL;
}());

/**
 * Stock object
 * @param {[type]} key
 * @param {[type]} value
 * @param {[type]} symbol
 * @param {[type]} name
 * @param {[type]} change
 * @param {[type]} changep
 * @param {[type]} state
 */
function Stock(key, value, symbol, name, change, changep, state) {
  this.key = key;
  this.symbol = symbol;
  this.name = name;
  this.change = change;
  this.value = value;
  this.changep = changep;
  this.state = state;
}
Stock.prototype = {
  constructor: Stock,
  logData: function () {
    console.log(this.change);
  }
};



/**
 * Returns the length of an object
 * @param  {[type]} obj
 * @return {[type]}
 * courtesy of http://stackoverflow.com/questions/5223/length-of-javascript-object-ie-associative-array
 */
Object.size = function (obj) {
  var size = 0,
    key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      size++;
    }
  }
  return size;
};


window.onload = function () {
  console.log("/-/-/-/-/-/-/-/-/-/-/");

  console.log("Starting...");
  sq.init();
};

